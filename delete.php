<!DOCTYPE html>
<html>
<head>
<title>Delete</title>
</head>
<body>

<?php
session_start();
$username = $_SESSION["username"];

$initfilename = $_GET['file'];
$filename = rtrim($initfilename, '/');

$file_path = sprintf("/media/Module2/userfiles/%s/%s", $username, $filename);

if (!unlink($file_path)) {
  echo ("Error deleting $file_path");
}
else {
  echo ("Deleted $file_path");
  header("Location: fileview.php");
}


?>

</body>
</html>
