<!DOCTYPE html>
<html>
<head>
<title>Dashboard</title>
</head>
<body>

<form action="fileshare-p2.php" method="POST">
    <input type="submit" name="logout" value="Logout"/>
</form>

<form action="fileview.php" method="POST">
    <input type="submit" name="filelink" value="View Files"/>
</form>


<form enctype="multipart/form-data" action="uploader.php" method="POST">
	<p>
		<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
		<label for="uploadfile_input">Choose a file to upload:</label> <input name="uploadedfile" type="file" id="uploadfile_input" />
	</p>
	<p>
		<input type="submit" value="Upload File" />
	</p>
</form>

</body>
</html>


<?php
session_start();
$username = $_SESSION["username"];

if (isset($_POST['logout'])) {
    session_unset();
    session_destroy();
    header("Location: fileshare-landing.php");
}

if (isset($_POST['filelink'])) {
    header("Location: fileview.php");
}


?>
