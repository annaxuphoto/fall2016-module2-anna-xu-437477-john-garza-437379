<!DOCTYPE html>
<html>
<head>
<title>Share</title>
</head>
<body>

<?php
session_start();
$username = $_SESSION["username"];

 
$initfilename = $_GET['file'];
$filename = rtrim($initfilename, '/');
 
// We need to make sure that the filename is in a valid format; if it's not, display an error and leave the script.
// To perform the check, we will use a regular expression.
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "Invalid filename";
	
	exit;
}
 
// Get the username and make sure that it is alphanumeric with limited other characters.
// You shouldn't allow usernames with unusual characters anyway, but it's always best to perform a sanity check
// since we will be concatenating the string to load files from the filesystem.
$username = $_SESSION['username'];
if( !preg_match('/^[\w_\-]+$/', $username) ){
	echo "Invalid username";
	exit;
}
 
$file_path = sprintf("/media/Module2/userfiles/%s/%s", $username, $filename);
 
// Now we need to get the MIME type (e.g., image/jpeg).  PHP provides a neat little interface to do this called finfo.
$finfo = new finfo(FILEINFO_MIME_TYPE);
$mime = $finfo->file($file_path);
 
// Finally, set the Content-Type header to the MIME type of the file, and display the file.
header("Content-Type: ".$mime);
readfile($file_path);

if(isset($_POST['logout'])) {
    session_unset();
    session_destroy();
    header("Location: fileshare-landing.php");
}

if(isset($_POST['back'])) {
    header("Location: fileview.php");
}
 
?>

</body>
</html>
