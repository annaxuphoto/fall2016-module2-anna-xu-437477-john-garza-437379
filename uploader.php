<!DOCTYPE html>
<html>
<head>
<title>Uploader</title>
</head>
<body>

<?php
session_start();
$username = $_SESSION["username"];

// Get the filename and make sure it is valid
$filename = basename($_FILES['uploadedfile']['name']);
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "Invalid filename";
	exit;
}
 
 
// Get the username and make sure it is valid
if( !preg_match('/^[\w_\-]+$/', $username) ){
	echo "Invalid username";
	exit;
}

$file_path = sprintf("/media/Module2/userfiles/%s/%s", $username, $filename);

if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $file_path) ){

	header("Location: fileview.php");
	exit;
}
else{
	echo "Upload failed";
	exit;
}
?>

</body>
</html>
