<!DOCTYPE html>
<html>
<head>
<title>Box Drop</title>
</head>
<body>
    <p>Welcome to Box Drop :)</p>
    <form action="fileshare-landing.php" method="POST">
        <label>Username: <input type = "text" name = "username"/></label>
        <input type = "submit" name = "submit" value = "Go"/>
        <label>Make a new user: <input type = "text" name = "newuser"/></label>
        <input type = "submit" name = "creation" value = "Create"/>
    </form>

</body>
</html>

<?php
if (isset($_POST['submit'])) { //check username
    
    $file = fopen("/media/Module2/users.txt", "r");
    
    $username = (string) $_POST['username'];
    $isValid  = false;
    
    while (!feof($file)) {
        $h = fgets($file);
        if ($username === trim($h)) {
            
            //move to next page
            $isValid = true;
        }
    }

    if (!$isValid) {
        //print wrong username, return to landing page
        echo "Incorrect username, try again.";
    } 
    else if ($isValid) {
        session_start();
        $_SESSION["username"] = $username;
        header("Location: fileshare-p2.php");
    }
}

if (isset($_POST['creation'])) {
    $username = (string) $_POST['newuser'];
    // Get the username and make sure it is valid
    if (!preg_match('/^[\w_\-]+$/', $username)) {
        echo "Invalid username";
        exit;
    } 
    else {
        $file = fopen("/media/Module2/users.txt", "r");
        
        while (!feof($file)) {
            $h = fgets($file);
            if ($username === trim($h)) {
                
                //check if this user already exists
                $alreadyexists = true;
                
            }
        }

        fclose($file);

        if ($alreadyexists) {
            echo "User already exists!";
        } 
        else {
            
            $file = fopen("/media/Module2/users.txt", "a");
            fwrite($file, "\n" . $username);
            fclose($file);
            
            $path = sprintf("/media/Module2/userfiles/%s/", $username);
            mkdir($path);
            echo "New user created!";
        }
        
    }
}

?>
