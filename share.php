<!DOCTYPE html>
<html>
<head>
<title>Share</title>
</head>
<body>

<?php

session_start();
$username = $_SESSION["username"];

$initfilename = $_GET['file'];
$filename     = rtrim($initfilename, '/');
$file_path    = sprintf("/media/Module2/userfiles/%s/%s", $username, $filename);

echo '<form action="share.php" method="GET"><label>Share ' . $filename . ' with: <input type="text" name="sharedwith"/></label><input type = "hidden" name = "file" value =' . $filename . '/><input type = "submit" name = "submit" value = "Go"/></form>';

if (isset($_GET['submit'])) { //check username
    
    $usernames = fopen("/media/Module2/users.txt", "r");
    
    $shareduser = $_GET['sharedwith'];
    $isValid    = false;
    
    while (!feof($usernames)) 
    {
        $h = fgets($usernames);
        if ($shareduser === trim($h)) {
            
            //move to next page
            $isValid = true;
        }
    }
    
    if (!$isValid) {
        echo "invalid username, try again.";
    } else if ($isValid) {
        
        $initfilename = $_GET['file'];
        $filename     = rtrim($initfilename, '/');
        
        $file_path = sprintf("/media/Module2/userfiles/%s/%s", $username, $filename);
        $new_path  = sprintf("/media/Module2/userfiles/%s/%s", $shareduser, $filename);
        
        if (copy($file_path, $new_path)) {
            echo "File shared!";
        } else {
            echo "File share error";
        }
    }
    
}
?>

</body>
</html>
