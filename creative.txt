Module 2: Anna Xu (437477), John Garza (437379)
Link to front page of site: http://ec2-54-234-16-18.compute-1.amazonaws.com/~johnegarza/fileshare-landing.php

For our creative portion, we implemented file sharing and and user creation. When a user shares a file with another existing user, the file is copied into the shared user's directory. When a new user is created, users.txt is update to include the new username, and a new directory is created for the new user. If a user attempts to create an existing user, the site informs the user that the username already exists.

The initial users.txt file includes the following usernames:

annaxu
johnegarza
herbertebutt
alexgoay
olivexu
cathyliu
kirankuttickat

New users can be created as well.